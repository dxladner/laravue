Vue.component('task-list', {
  template: `
  <div>
  <ul class="list-group">
    <task v-for="task in tasks">{{task.description}}</task>
  </ul>
  </div>
  `,
  data() {
    return {
      tasks: [
        {description: 'Go to church', completed: true},
        {description: 'Wash car', completed: true},
        {description: 'Finish clothes', completed: false},
        {description: 'Go to store', completed: true},
        {description: 'Clean room', completed: false},
      ]

    };
  }
});

Vue.component('task', {
  template: `

    <li class="list-group-item"><slot></slot></li>

  `
});

new Vue({
  el: '#root',

});
