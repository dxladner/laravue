Vue.component('bulma-article', {
  props: ['title', 'body'],
  template: `
  <article class="message" v-show="isVisible">
    <div class="message-header">
    {{ title }}
      <button class="delete" v-on:click="hideModal"></button>
    </div>
    <div class="message-body">
    {{ body }}
    </div>
  </article>
  `,
  data() {
    return {
      isVisible: true
    }
  },
  methods: {
    hideModal() {
      this.isVisible = false;
    }
  }

});

new Vue({
    el: '#root'

});
